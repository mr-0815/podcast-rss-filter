import datetime
import logging

import pytest
from feedendum import FeedItem

from podfilter.app import apply_filters
from podfilter.validation.models import FilterModel


class TestApplyFilters:
    @pytest.fixture(scope="class")
    def item_sample(self):
        return FeedItem(
            title="First Item",
            update=datetime.datetime(
                # weekday is 2
                2024,
                6,
                26,
                20,
                15,
                37,
                tzinfo=datetime.UTC,
            ),
        )

    @pytest.mark.parametrize(
        ("filters", "expected_is_filtered_out"),
        [
            ([{"title": "Item", "weekday": None}], False),
            ([{"title": "foo", "weekday": None}], True),
            ([{"title": "", "weekday": None}], False),
            (
                [{"title": "Item", "weekday": None}, {"title": "foo", "weekday": None}],
                False,
            ),
            (
                [{"title": "foo", "weekday": None}, {"title": "Item", "weekday": None}],
                False,
            ),
        ],
    )
    def test_title_filter(self, item_sample, filters, expected_is_filtered_out):
        filters = [FilterModel(**item_filters) for item_filters in filters]
        is_filtered_out, _ = apply_filters(filters, item_sample)

        # should generate expected result
        assert is_filtered_out is expected_is_filtered_out

    @pytest.mark.parametrize(
        ("filters", "expected_is_filtered_out"),
        [
            # weekday in sample data is 2
            ([{"title": "Item", "weekday": None}], False),
            ([{"title": "Item", "weekday": 0}], True),
            ([{"title": "Item", "weekday": 2}], False),
            ([{"title": "Item", "weekday": 4}], True),
            ([{"title": "Item", "weekday": [4]}], True),
            ([{"title": "Item", "weekday": [1, 2, 3]}], False),
            ([{"title": "Item", "weekday": [4, 6]}], True),
            ([{"title": "", "weekday": None}], False),
            ([{"title": "", "weekday": 2}], False),
            ([{"title": "", "weekday": 4}], True),
            ([{"title": "Second", "weekday": None}], True),
            ([{"title": "Second", "weekday": 2}], True),
            ([{"title": "Second", "weekday": 4}], True),
        ],
    )
    def test_weekday_condition(self, item_sample, filters, expected_is_filtered_out):
        filters = [FilterModel(**item_filters) for item_filters in filters]
        is_filtered_out, _ = apply_filters(filters, item_sample)

        # should generate expected result
        assert is_filtered_out is expected_is_filtered_out

    @pytest.mark.parametrize(
        ("filter_title", "filter_weekday", "expected_is_filtered_out"),
        [("Item", 0, False), ("Item", 1, True), ("foo", 0, True), ("foo", 1, True)],
    )
    def test_weekday_in_item_is_0(
        self, filter_title, filter_weekday, expected_is_filtered_out
    ):
        item = FeedItem(
            title="Item",
            update=datetime.datetime(
                # weekday is 0
                2024,
                6,
                24,
                20,
                15,
                37,
                tzinfo=datetime.UTC,
            ),
        )
        data = {"title": filter_title, "weekday": filter_weekday}
        filters = [FilterModel(**data)]
        is_filtered_out, _ = apply_filters(filters, item)
        assert is_filtered_out is expected_is_filtered_out

    @pytest.mark.parametrize(
        "item_update",
        [None, ""],
    )
    def test_invalid_date_in_item(self, caplog, item_update):
        item = FeedItem(title="Item", update=item_update)

        data = {"title": "Item", "weekday": 1}
        filters = [FilterModel(**data)]
        apply_filters(filters, item)

        # should log the expected error
        assert (
            "podfilter.app",
            logging.WARNING,
            f"Ignoring invalid update object in '{item.title!s}'",
        ) in caplog.record_tuples
