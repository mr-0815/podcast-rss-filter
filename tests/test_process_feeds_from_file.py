import logging
from unittest.mock import patch

from pydantic_core import Url

from podfilter.app import process_feeds_from_file
from podfilter.validation.models import FeedListModel, FeedModel


@patch("podfilter.app.handle_feed_url")  # mock_handler
class TestProcessFeedsFromFile:
    def test_success(self, mock_handler):
        feeds = [
            {
                "title": "Item",
                "url": "https://first-url.com/podcast.xml",
                "filter": None,
                "max_age_hours": 0,
                "hosted_url": None,
            },
            {
                "title": "Item",
                "url": "https://second-url.com/podcast.xml",
                "filter": None,
                "max_age_hours": 0,
                "hosted_url": None,
            },
        ]
        feed_list = [FeedModel(**feed) for feed in feeds]
        return_value = FeedListModel(feeds=feed_list)
        with patch("podfilter.app.get_all_feeds", return_value=return_value):
            files = process_feeds_from_file("use mocked values")

        # should generate the expected output
        assert len(files) == 2

        # should call handler for each of the feeds
        assert mock_handler.call_count == 2

        # should create the expected values
        for url in [
            "https://first-url.com/podcast.xml",
            "https://second-url.com/podcast.xml",
        ]:
            expected_args = {
                "url": Url(url),
                "filters": None,
                "max_age_hours": 0,
                "hosted_url": None,
            }
            mock_handler.assert_any_call(**expected_args)

    def test_no_feeds(self, mock_handler, caplog):
        filename = "foo"
        with patch("podfilter.app.get_all_feeds", return_value=None):
            files = process_feeds_from_file(filename, _is_module=False)

        # should generate the expected output
        assert files == []

        # should perform an early return
        mock_handler.assert_not_called()

        # should log the expected error
        assert (
            "podfilter.app",
            logging.ERROR,
            f"Invalid input file: {filename}",
        ) in caplog.record_tuples
