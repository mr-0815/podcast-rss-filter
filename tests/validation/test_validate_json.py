import json
import logging

import pytest
from pydantic_core import Url

from podfilter.validation.models import FeedListModel, FeedModel, FilterModel
from podfilter.validation.validate_json import validate_json_data


@pytest.mark.usefixtures("caplog_at_debug_level")
class TestValidateJsonData:
    @pytest.fixture(scope="class")
    def data_ok(self):
        filename = "tests/assets/feeds.json"
        with open(filename) as fh:
            return json.loads(fh.read())

    @pytest.fixture(scope="class")
    def data_invalid(self):
        filename = "tests/assets/invalid.json"
        with open(filename) as fh:
            return json.loads(fh.read())

    def test_validation_success(self, data_ok, caplog):
        result = validate_json_data(data_ok)

        # should return the expected output
        assert isinstance(result, FeedListModel)
        assert isinstance(result.feeds[0], FeedModel)
        assert result.feeds[0].filter
        assert isinstance(result.feeds[0].filter[0], FilterModel)
        assert result.feeds[1].filter is None
        assert isinstance(result.feeds[0].title, str)
        assert isinstance(result.feeds[0].url, Url)
        assert isinstance(result.feeds[0].hosted_url, Url)
        assert result.feeds[1].hosted_url is None
        assert isinstance(result.feeds[1].max_age_hours, int)
        assert isinstance(result.feeds[2].max_age_hours, float)
        assert isinstance(result.feeds[0].filter[0].title, str)
        assert result.feeds[0].filter[0].weekday
        assert isinstance(result.feeds[0].filter[0].weekday[0], int)

        # should log the expected success message
        assert (
            "podfilter.validation.validate_json",
            logging.DEBUG,
            "Input validation succeeded.",
        ) in caplog.record_tuples

    def test_validation_failed(self, data_invalid, caplog):
        result = validate_json_data(data_invalid)

        # should return the expected output
        assert result is None

        # should not log success
        assert "Input validation succeeded." not in caplog.text

    @pytest.mark.parametrize("weekday_filter", [1, [1], "1", 0, 6, None])
    def test_weekday_list_success(self, weekday_filter, caplog):
        data = [
            {
                "title": "First Feed",
                "url": "https://example.com/podcast_1.xml",
                "filter": [{"title": ""}],
            },
        ]
        data[0]["filter"][0]["weekday"] = weekday_filter

        result = validate_json_data(data)

        # should log the expected output
        assert "Input validation succeeded." in caplog.text

        # should generate the expected result
        assert isinstance(result, FeedListModel)
        if weekday_filter is not None:
            assert result.feeds
            assert result.feeds[0].filter
            assert isinstance(result.feeds[0].filter[0].weekday, list)

    @pytest.mark.parametrize("weekday_filter", ["foo", ["foo"], -1, 7, 1.5])
    def test_weekday_list_failed(self, weekday_filter, caplog):
        data = [
            {
                "title": "First Feed",
                "url": "https://example.com/podcast_1.xml",
                "filter": [{"title": ""}],
            },
        ]
        data[0]["filter"][0]["weekday"] = weekday_filter

        validate_json_data(data)

        # should not log success
        assert "Input validation succeeded." not in caplog.text

    @pytest.mark.parametrize(
        ("title", "url", "hosted_url", "filter_args", "expected_error"),
        [
            (
                "foo",
                "https://example.com/foo.xml",
                None,
                [{}],
                "Field required at ('feeds', 0, 'filter', 0, 'title')",
            ),
            ("foo", "bar", None, [], "Input should be a valid URL"),
            ("foo", "https://example.com", "bar", [], "Input should be a valid URL"),
            (
                "foo",
                "bar",
                None,
                [{"title": "baz", "weekday": "foo"}],
                "unable to parse string as an integer at",
            ),
            (
                "foo",
                "bar",
                None,
                [{"title": "baz", "weekday": 23}],
                "Input should be less than or equal to 6",
            ),
            (
                "foo",
                "bar",
                None,
                [{"title": "baz", "weekday": -3}],
                "Input should be greater than or equal to 0 at",
            ),
        ],
    )
    def test_error_message(
        self, title, url, hosted_url, filter_args, expected_error, caplog
    ):
        data = [
            {
                "title": title,
                "url": url,
                "hosted_url": hosted_url,
                "filter": filter_args,
            },
        ]

        validate_json_data(data)
        assert "ValidationError" in caplog.text
        assert expected_error in caplog.text

        if expected_error == "Field required":
            assert "- input was" not in caplog.text
