import pytest
from pydantic import ValidationError

from podfilter.validation.models import FilterModel, convert_to_list


class TestModel:
    @pytest.mark.parametrize(
        ("v", "expected_result"),
        [
            (None, None),
            (23, [23]),
            ([1, 2, 3], [1, 2, 3]),
        ],
    )
    def test_convert_to_list(self, v, expected_result):
        assert convert_to_list(v) == expected_result

    @pytest.mark.parametrize(
        "weekday",
        [
            None,
            [1, 2, 3],
        ],
    )
    def test_beforevalidator_success(self, weekday):
        FilterModel(title="foo", weekday=weekday)

    @pytest.mark.parametrize(
        "weekday",
        [
            23,
        ],
    )
    def test_beforevalidator_failed(self, weekday):
        with pytest.raises(ValidationError):
            FilterModel(title="foo", weekday=weekday)
