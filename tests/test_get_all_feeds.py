import errno
import logging
from unittest.mock import patch

import pytest

from podfilter.app import get_all_feeds, process_feeds_from_file
from podfilter.validation.models import FeedListModel, FeedModel


class TestGetAllFeeds:
    def test_filename_ok(self):
        filename = "tests/assets/feeds.json"
        feeds = get_all_feeds(filename)
        assert feeds
        assert isinstance(feeds, FeedListModel)
        assert all(isinstance(feed, FeedModel) for feed in feeds.feeds)

    @pytest.mark.parametrize(
        ("filename", "expected_error"),
        [
            (
                "non_existent_file.json",
                "Error: File 'non_existent_file.json' not found.",
            ),
            (
                "tests/assets/empty.txt",
                "Error: File 'tests/assets/empty.txt' is not a valid JSON file.",
            ),
        ],
    )
    def test_filename_errors(self, caplog, filename, expected_error):
        with pytest.raises(Exception) as e:  # noqa: PT012, PT011
            feeds = get_all_feeds(filename)

            # should return an empty list
            assert feeds == []

            # should not raise an exception
            assert not e.value

        # should log the expected error
        assert ("podfilter.app", logging.ERROR, expected_error) in caplog.record_tuples

        # should not raise any exceptions in handler function
        with pytest.raises(Exception) as e:  # noqa: PT012, PT011
            process_feeds_from_file(filename)
            assert not e.value

    def test_os_error(self, caplog):
        with patch("builtins.open") as mock_open:
            mock_open.side_effect = OSError(errno.EACCES, "Permission denied")
            filename = "tests/assets/feeds.json"
            feeds = get_all_feeds(filename)

        # should not create a feeds object
        assert feeds is None

        # should log the expected error
        assert "Permission denied" in caplog.text
