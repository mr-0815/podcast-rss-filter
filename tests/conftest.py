import logging
from unittest.mock import patch

import pytest

from podfilter.app import logger
from podfilter.util.app_args import AppArgs


@pytest.fixture
def caplog_at_debug_level(caplog):
    caplog.set_level(logging.DEBUG)


@pytest.fixture(autouse=True)
def reset_args():
    default_args = AppArgs(
        filename="",
        log_level="INFO",
        outdir="output",
        write_always=False,
    )
    with patch("podfilter.app.args", new=default_args):
        yield


@pytest.fixture(autouse=True)
def reset_logger():
    """Clear the logger's handlers for each test."""
    logger.handlers.clear()

    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        fmt="%(asctime)s %(levelname)s: %(message)s", datefmt="%Y-%m-%d %H:%M:%S"
    )
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    logger.setLevel(logging.DEBUG)

    yield

    logger.handlers.clear()
