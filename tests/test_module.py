from unittest.mock import patch

import pytest
from pydantic_core import Url

import podfilter
import podfilter.app
from podfilter.util.logging_setup import ApplicationError
from podfilter.validation.models import FeedListModel, FeedModel


class TestModule:
    def test_no_filename(self):
        with pytest.raises(TypeError):
            podfilter.process_feeds_from_file()

    @patch("podfilter.app.get_all_feeds", return_value=None)
    def test_no_result(self, mock_get_all, caplog):
        with pytest.raises(ApplicationError, match="Invalid input file:"):
            podfilter.process_feeds_from_file("filename")

    @patch("podfilter.app.get_all_feeds", return_value=FeedListModel(feeds=[]))
    def test_result(self, mock_get_all):
        result = podfilter.process_feeds_from_file("filename")
        assert result == []

    @patch("podfilter.app.handle_feed_url", return_value="filename")
    @patch(
        "podfilter.app.get_all_feeds",
        return_value=FeedListModel(
            feeds=[FeedModel(title="title", url=Url("http://filename"))]
        ),
    )
    def test_success(self, mock_get_all, mock_handle):
        result = podfilter.process_feeds_from_file("filename")
        assert result == ["filename"]

    @patch("podfilter.app.get_all_feeds", return_value=FeedListModel(feeds=[]))
    def test_args(self, mock_get_all):
        with patch("podfilter.app.args") as mock_args:
            podfilter.process_feeds_from_file(
                "filename", outdir="new_outdir", write_always=True
            )

            assert mock_args.outdir == "new_outdir"
            assert mock_args.write_always is True
