from dataclasses import asdict
from unittest.mock import patch

import podfilter
from podfilter.app import main


class TestMainFunction:
    cli_args = {  # noqa: RUF012
        "filename": "foo",
        "log_level": "DEBUG",
        "outdir": "new_outdir",
        "write_always": True,
    }

    @patch("podfilter.app.process_feeds_from_file")
    @patch("podfilter.app.setup_cli_args", return_value=cli_args)
    def test_main_function(self, mock_setup_cli_args, mock_process_feeds):
        main()

        # should pass the expected arguments
        mock_setup_cli_args.assert_called_once()
        mock_process_feeds.assert_called_with("foo", _is_module=False)

        # should set the expected AppArgs
        args = podfilter.app.args
        assert asdict(args) == {
            "filename": "foo",
            "log_level": "DEBUG",
            "outdir": "new_outdir",
            "write_always": True,
        }
