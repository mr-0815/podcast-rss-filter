import logging
from datetime import datetime, timedelta, timezone
from unittest.mock import patch

import pytest
from feedendum import Feed, from_rss_file
from pydantic_core import Url

from podfilter.app import filter_feed
from podfilter.validation.models import FilterModel


class TestFilterFeed:
    @pytest.fixture(scope="class")
    def feed_sample(self) -> Feed:
        filename = "tests/assets/feed.xml"
        return from_rss_file(filename)

    @pytest.fixture(scope="class")
    def feed_sample_relative(self) -> Feed:
        """Sets first item to 10 hours ago."""
        filename = "tests/assets/feed.xml"
        feed: Feed = from_rss_file(filename)

        new_date = datetime.now(tz=timezone(timedelta(seconds=7200))).replace(
            minute=0, second=0, microsecond=0
        ) - timedelta(hours=10)
        feed.items[0].update = new_date
        return feed

    @pytest.mark.parametrize(
        ("filters", "expected_len_items"),
        [
            ([{"title": "", "weekday": None}], 2),
            ([{"title": "First", "weekday": None}], 1),
            ([{"title": "Second", "weekday": None}], 1),
            ([{"title": "Item", "weekday": None}], 2),
        ],
    )
    def test_filter_no_limit(self, feed_sample, filters, expected_len_items):
        max_age_hours = 0

        filter_args = [FilterModel(**item_filters) for item_filters in filters]
        result = filter_feed(feed_sample, filter_args, max_age_hours)

        # should generate valid feed
        assert isinstance(result, Feed)
        assert result.title == "Podcast Dummy"
        assert result._data == feed_sample._data  # noqa: SLF001

        # should add the expected items to the output feed
        assert len(result.items) == expected_len_items

        for i, filter_args in enumerate(filters[:expected_len_items]):
            assert filter_args["title"] in result.items[i].title

    @pytest.mark.parametrize(
        ("max_age_hours", "expected_len_items"),
        [
            (0, 2),  # disables checking
            (-1, 0),
            (9, 0),  # first item is 10 hours ago
            (10, 0),
            (11, 1),
            ("11", 1),
            (24, 1),  # second item is long ago
            (1_000_000, 2),  # but not that long
        ],
    )
    def test_max_age(self, feed_sample_relative, max_age_hours, expected_len_items):
        filter_titles = []
        result = filter_feed(feed_sample_relative, filter_titles, max_age_hours)
        assert len(result.items) == expected_len_items

    @pytest.mark.parametrize(
        ("filters", "max_age_hours", "expected_len_items"),
        [
            ([{"title": "", "weekday": None}], 0, 2),
            ([{"title": "First", "weekday": None}], 0, 1),
            ([{"title": "Second", "weekday": None}], 0, 1),
            ([{"title": "Item", "weekday": None}], 0, 2),
            ([{"title": "", "weekday": None}], 3, 0),
            ([{"title": "First", "weekday": None}], 3, 0),
            ([{"title": "Second", "weekday": None}], 3, 0),
            ([{"title": "Item", "weekday": None}], 3, 0),
            ([{"title": "", "weekday": None}], 24, 1),
            ([{"title": "First", "weekday": None}], 24, 1),
            ([{"title": "Second", "weekday": None}], 24, 0),
            ([{"title": "Item", "weekday": None}], 24, 1),
            ([{"title": "", "weekday": None}], 1_000_000, 2),
            ([{"title": "First", "weekday": None}], 1_000_000, 1),
            ([{"title": "Second", "weekday": None}], 1_000_000, 1),
            ([{"title": "Item", "weekday": None}], 1_000_000, 2),
        ],
    )
    def test_filter_and_max_age(
        self, feed_sample_relative, filters, max_age_hours, expected_len_items
    ):
        filter_args = [FilterModel(**item_filters) for item_filters in filters]
        result = filter_feed(feed_sample_relative, filter_args, max_age_hours)
        assert len(result.items) == expected_len_items

    def test_malformed_date_string(self, caplog):
        filename = "tests/assets/feed_bad_date.xml"
        feed: Feed = from_rss_file(filename)
        result = filter_feed(feed, [], 24)

        # should log the expected error
        for input_str in [
            "None",
            "2024-06-26 20:15:37",
        ]:
            expected_error = f"Could not parse date string {input_str} using %Y-%m-%d %H:%M:%S%z. Adding this item anyway."
            assert (
                "podfilter.app",
                logging.WARNING,
                expected_error,
            ) in caplog.record_tuples

        # should add the items anyway
        assert len(result.items) == 2

    def test_feed_url(self, feed_sample):
        new_feed_url = Url("https://example.com/my-feed.xml")

        with patch("podfilter.app.parse_feed_data") as mock_parse_feed_data:
            filter_feed(feed_sample, None, 0, new_feed_url)

        mock_parse_feed_data.assert_called_with(
            data=feed_sample._data,  # noqa: SLF001
            new_feed_url=new_feed_url,
        )
