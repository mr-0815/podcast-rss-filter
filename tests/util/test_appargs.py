import argparse
from unittest.mock import patch

import pytest

from podfilter.util import defaults
from podfilter.util.app_args import AppArgs, setup_cli_args


class TestAppArgs:
    @pytest.fixture
    def cli_args(self):
        return {
            "filename": "foo",
            "log_level": "DEBUG",
            "outdir": "new_outdir",
            "write_always": True,
        }

    def test_app_args_defaults(self):
        args = AppArgs()

        # should return an AppArgs instance
        assert isinstance(args, AppArgs)

        # should return the expected values

    def test_app_args_passed_values(self, cli_args):
        args = AppArgs(**cli_args)

        # should return the expected values
        assert args.filename == "foo"
        assert args.log_level == "DEBUG"
        assert args.outdir == "new_outdir"
        assert args.write_always is True

    def test_update(self, cli_args):
        args = AppArgs()
        assert args.filename == ""
        assert args.log_level == "INFO"
        assert args.outdir == "output"
        assert args.write_always is False

        args.filename = "new"
        # should change the value
        assert args.filename == "new"

        args.update(**cli_args)

        # should return the updated values
        assert args.filename == "foo"
        assert args.log_level == "DEBUG"
        assert args.outdir == "new_outdir"
        assert args.write_always is True


class TestSetupCliArgs:
    @pytest.fixture
    def parser(self):
        return argparse.ArgumentParser()

    def test_setup_args_defaults(self, parser):
        cli_args = ["app.py", "filename"]
        expected_args = {
            "filename": "filename",
            "outdir": defaults.OUTDIR,
            "write_always": False,
            "log_level": defaults.LOG_LEVEL,
        }

        with patch("sys.argv", cli_args):
            args_result = setup_cli_args()

        # should generate a dict from argparse namespace
        assert isinstance(args_result, dict)

        # dict should contain expected arguments
        assert args_result == expected_args

    @pytest.mark.parametrize(
        ("arg_key", "arg_var", "arg_value"),
        [
            ("--outdir", "outdir", "foo"),
            ("--log-level", "log_level", "DEBUG"),
        ],
    )
    def test_setup_args(self, parser, arg_key, arg_var, arg_value):
        cli_args = ["app.py", arg_key, arg_value, "filename"]
        expected_args = {arg_var: arg_value}

        with patch("sys.argv", cli_args):
            args_result = setup_cli_args()

        # dict should contain expected arguments
        assert args_result.get(arg_var) == expected_args.get(arg_var)

    def test_setup_boolean(self, parser):
        cli_args = ["app.py", "--write-always", "filename"]

        with patch("sys.argv", cli_args):
            args_result = setup_cli_args()

        # dict should contain expected arguments
        assert args_result.get("write_always") is True
