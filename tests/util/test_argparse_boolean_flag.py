import argparse

import pytest

from podfilter.util.argparse_boolean_flag import BooleanFlag


class TestBooleanFlag:
    @pytest.fixture
    def parser(self):
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "--write-always",
            action=BooleanFlag,
            default=False,
            help="write output file to disk, even if the content was not changed",
        )
        return parser

    def test_default_value(self, parser):
        args = parser.parse_args([])

        # should default to False
        assert not args.write_always

    def test_set_to_true(self, parser):
        args = parser.parse_args(["--write-always"])

        # should now be True
        assert args.write_always

    def test_help_message(self, parser):
        help_message = parser.format_help()
        assert "--write-always" in help_message
        assert "write output file to disk, even if the content was not" in help_message
        assert "no-write-always" not in help_message
