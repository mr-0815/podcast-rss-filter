import logging
from datetime import datetime, timedelta

import pytest
import pytz

from podfilter.util.helpers import is_older_than_hours


class TestIsOlderThanHours:
    @pytest.fixture
    def old_datetime(self):
        date_format = "%Y-%m-%d %H:%M:%S%z"
        timestamp = "2020-01-01 12:00:00+0200"
        return datetime.strptime(timestamp, date_format)  # noqa: DTZ007

    @pytest.fixture
    def datetime_10_hours(self):
        now = datetime.now(pytz.timezone("UTC"))
        return now - timedelta(hours=10)

    def test_ok_old(self, old_datetime):
        # should accept valid hours and return True
        for hours in [24, 1, 0, -2, 1.5, "23"]:
            result = is_older_than_hours(old_datetime, hours)
            assert result is True

    def test_ok_new(self, datetime_10_hours):
        result = is_older_than_hours(datetime_10_hours, 24)
        assert result is False

        for hours in [10, 1, 0, -2, 1.5, "2"]:
            result = is_older_than_hours(datetime_10_hours, hours)
            assert result is True

    def test_ok_timezones(self):
        now_local = datetime.now(pytz.timezone("Europe/Vienna"))  # UTC+2
        now_utc = datetime.now(pytz.timezone("UTC"))
        now_us = datetime.now(pytz.timezone("US/Eastern"))  # UTC-5

        # should handle timezones correctly
        for dt in [now_local, now_utc, now_us]:
            assert is_older_than_hours(dt, 0) is True

            for hours in [0.1, 1, 2, 24]:
                assert is_older_than_hours(dt, hours) is False

    def test_invalid_hours(self, caplog, old_datetime):
        hours = "foo"
        expected_error = f"Invalid argument: hours={hours}"

        # should reject invalid hour args and return False
        with pytest.raises(Exception) as e:  # noqa: PT012, PT011
            result = is_older_than_hours(old_datetime, hours)
            assert result is False

            # should not raise an exception
            assert not e.value

        # should log the expected error
        assert (
            "podfilter.util.helpers",
            logging.ERROR,
            expected_error,
        ) in caplog.record_tuples
