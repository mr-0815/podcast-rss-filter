import logging

import pytest
from feedendum import Feed, from_rss_file
from pydantic_core import Url

from podfilter.util.feed_data import parse_feed_data


@pytest.mark.usefixtures("caplog_at_debug_level")
class TestFeedData:
    @pytest.fixture(scope="class")
    def feed_sample(self) -> Feed:
        filename = "tests/assets/feed.xml"
        return from_rss_file(filename)

    def test_bad_input_format(self):
        filename = "tests/assets/feed_with_comment.xml"
        feed: Feed = from_rss_file(filename)
        result = parse_feed_data(feed._data)  # noqa: SLF001
        for item in result:
            assert isinstance(item, str)

    def test_no_custom_url(self, feed_sample):
        data = feed_sample._data  # noqa: SLF001

        # should not change the data
        result = parse_feed_data(data)
        assert result == data

    @pytest.mark.parametrize("data", [{"key": "value"}, {}])
    def test_no_url_in_data_no_custom_url_passed(self, data):
        # should not change the data
        assert parse_feed_data(data) == data

    @pytest.mark.parametrize("data", [{"key": "value"}, {}])
    def test_no_url_in_data(self, data, caplog):
        new_feed_url = Url("http://example.com")
        # should not change the data
        assert parse_feed_data(data, new_feed_url=new_feed_url) == data

        # should log the expected output
        assert (
            "podfilter.util.feed_data",
            logging.INFO,
            f"Could not inject {new_feed_url!s}: No target found.",
        ) in caplog.record_tuples

    @pytest.mark.parametrize("new_url", ["https://my-server.com/podcast.xml"])
    def test_custom_url(self, feed_sample, new_url, caplog):
        # validate test setup
        orig_url = "https://podcast.example.com/feed.xml"
        new_feed_url_key = "{http://www.itunes.com/dtds/podcast-1.0.dtd}new-feed-url"
        data = feed_sample._data  # noqa: SLF001
        assert data.get(new_feed_url_key) == orig_url

        result = parse_feed_data(data, Url(new_url))

        # should change the data
        assert result != data

        # should generate the expected result
        assert result[new_feed_url_key] == new_url

        # should log the expected output
        assert (
            "podfilter.util.feed_data",
            logging.DEBUG,
            f"Replacing {new_feed_url_key}: {data[new_feed_url_key]} -> {new_url}",
        ) in caplog.record_tuples

    def test_replace_in_atom_feed(self, caplog):
        feed: Feed = from_rss_file("tests/assets/atom_feed.xml")
        result = parse_feed_data(feed._data, new_feed_url=Url("http://foo"))  # noqa: SLF001

        # should generate the expected output
        key = "{http://www.w3.org/2005/Atom}link"
        assert result[key] == "[{'@href': 'http://foo/', '@rel': 'alternate'}]"

        # should log the expected output
        expected_message = "Replacing {http://www.w3.org/2005/Atom}link: [{'@href': 'https://pubsubhubbub.appspot.com/', '@rel': 'hub'}, {'@href': 'https://example.podigee.io/feed/mp3', '@rel': 'self'}, {'@href': 'https://example.podigee.io/feed/mp3', '@rel': 'first'}, {'@href': 'https://example.podigee.io/feed/mp3?page=1', '@rel': 'last'}] -> [{'@href': 'http://foo/', '@rel': 'alternate'}]"

        assert (
            "podfilter.util.feed_data",
            logging.DEBUG,
            expected_message,
        ) in caplog.record_tuples
