import logging

import pytest

from podfilter.util.logging_setup import (
    ApplicationError,
    logging_basic_config,
    raise_on_error,
    set_log_level,
)


class TestLoggingSetup:
    def test_basic_config(self):
        logger = logging.getLogger("testing1")
        assert logger.name == "testing1"
        assert logger.handlers == []

        logging_basic_config(logger)

        # should register the expected handler
        handler: logging.Handler = logger.handlers[0]
        assert handler
        assert isinstance(handler, logging.StreamHandler)

        # should register the expected formatter
        formatter: logging.Formatter = handler.formatter
        assert formatter
        assert formatter._fmt == "%(asctime)s %(levelname)s: %(message)s"  # noqa: SLF001
        assert formatter.datefmt == "%Y-%m-%d %H:%M:%S"

    @pytest.mark.parametrize("level", ["DEBUG", "debug"])
    def test_set_log_level(self, level):
        logger = logging.getLogger("testing2")
        logger.setLevel(logging.NOTSET)

        # should set the expected log level
        set_log_level(logger, level)
        assert logger.level == logging.DEBUG

    @pytest.mark.parametrize("level", ["foo", 23, "", None])
    def test_set_invalid_log_level(self, level, caplog):
        logger = logging.getLogger("testing3")
        logger.setLevel(logging.NOTSET)

        # should set the fallback value "INFO"
        set_log_level(logger, level)
        assert logger.level == logging.INFO

        # should log the expected error
        assert (
            f"Invalid log level: '{level}'. Using default value: 'INFO'" in caplog.text
        )

    def test_raise_on_error(self, caplog):
        logger = logging.getLogger("testing4")
        logging_basic_config(logger)
        logger.setLevel(logging.INFO)

        # should not raise an exception with basic config
        msg = "Should just log the error."
        with pytest.raises(Exception) as e:  # noqa: PT011, PT012
            logger.error(msg)
            assert not e
        assert msg in caplog.text

        raise_on_error(logger)

        # should raise an exception now
        msg = "Should raise an exception."
        with pytest.raises(ApplicationError, match=msg):
            logger.error(msg)
