import errno
import logging
import os
from unittest.mock import MagicMock, mock_open, patch
from urllib.parse import urlparse

import pytest
from pydantic_core import Url

from podfilter import app
from podfilter.app import write_output_to_file


class TestWriteOutputToFile:
    @pytest.fixture(autouse=True)
    def use_tmp_outdir(self, tmp_path):
        with patch.object(app.args, "outdir", tmp_path / "output"):
            yield

    @pytest.fixture
    def args(self, tmp_path):
        return {
            "output": "some dummy content",
            "url": Url("https://example.com/podcast.xml"),
            "outdir": tmp_path / "output",
        }

    def test_success(self, args):
        filename = write_output_to_file(args["output"], args["url"])
        assert filename
        assert filename.endswith("/output/podcast.xml")

    @pytest.mark.parametrize(
        "url", ["https://podcast.net/episodes/feed/", "http://foo/", "http://foo"]
    )
    def test_filename_creation(self, args, url):
        args["url"] = Url(url)
        filename = write_output_to_file(args["output"], args["url"])
        assert filename

    def test_filename_with_uncommon_url(self, args):
        args["url"] = urlparse("foo")

        # should create a file anyway
        filename = write_output_to_file(args["output"], args["url"])
        assert filename
        assert filename.endswith("/output/foo")

    def test_custom_filename(self, args):
        hosted_url = Url("https://example.com/my-filtered-podcast.xml")
        filename = write_output_to_file(
            args["output"], args["url"], hosted_url=hosted_url
        )

        assert filename
        assert filename.endswith("my-filtered-podcast.xml")

    @pytest.mark.parametrize(
        ("arg_key", "arg_value"),
        [
            ("output", ""),
            ("output", None),
            ("url", ""),
            ("url", None),
        ],
    )
    def test_missing_arguments(self, caplog, args, arg_key, arg_value):
        args[arg_key] = arg_value
        filename = write_output_to_file(args["output"], args["url"])

        # should not create a file
        assert filename is None

        # should log the expected error
        assert (
            "podfilter.app",
            logging.ERROR,
            f"Missing argument(s): {arg_key}",
        ) in caplog.record_tuples

    def test_no_arguments(self, caplog):
        args = ["", ""]
        filename = write_output_to_file(*args)

        # should not create a file
        assert filename is None

        # should log the expected error
        assert (
            "podfilter.app",
            logging.ERROR,
            "Missing argument(s): output, url",
        ) in caplog.record_tuples

    def test_conditional_write_unchanged(self, args, caplog):
        filename = f"{args["outdir"]}/{args["url"].path.rsplit('/')[-1]}"

        # mock unchanged file
        m: MagicMock = mock_open(read_data=args["output"])
        with patch("builtins.open", m):
            result = write_output_to_file(args["output"], args["url"])

        # should return the filename
        assert result == filename

        # should have called `open` once for read
        m.assert_called_once_with(filename)

        # should log the expected message
        assert (
            "podfilter.app",
            logging.INFO,
            f"File '{filename}' unchanged.",
        ) in caplog.record_tuples

    def test_conditional_write_has_changes(self, args, caplog):
        filename = f"{args["outdir"]}/{args["url"].path.rsplit('/')[-1]}"

        # mock changed file
        m: MagicMock = mock_open(read_data="other previous content")
        with patch("builtins.open", m):
            result = write_output_to_file(args["output"], args["url"])

        # should return the filename
        assert result == filename

        # should have called `open` for read + write
        m.assert_any_call(filename)
        m.assert_any_call(filename, "w")

        # should log the expected messages
        assert (
            "podfilter.app",
            logging.INFO,
            f"File '{filename}' unchanged.",
        ) not in caplog.record_tuples

        assert (
            "podfilter.app",
            logging.INFO,
            f"Writing file '{filename}'",
        ) in caplog.record_tuples

    @pytest.mark.parametrize(
        "read_data",
        [
            "some dummy content",  # mock unchanged file
            "other previous content",  # mock changes in file
        ],
    )
    def test_write_always(self, args, read_data, caplog):
        filename = f"{args["outdir"]}/{args["url"].path.rsplit('/')[-1]}"

        # mock file with parametrize
        m: MagicMock = mock_open(read_data=read_data)
        with patch("builtins.open", m):
            # set write_always_local_override
            write_output_to_file(
                args["output"], args["url"], write_always_local_override=True
            )

        # should have called `open` once for write
        m.assert_called_once_with(filename, "w")

        # should log the expected messages
        assert (
            "podfilter.app",
            logging.INFO,
            f"File '{filename}' unchanged.",
        ) not in caplog.record_tuples

        assert (
            "podfilter.app",
            logging.INFO,
            f"Writing file '{filename}'",
        ) in caplog.record_tuples

    def test_outdir_creation(self, args):
        filename_1 = write_output_to_file(args["output"], args["url"])
        assert filename_1

        # should create directory on disk
        assert os.path.exists(filename_1)
        dirname_1 = os.path.dirname(filename_1)
        assert os.path.isdir(dirname_1)
        assert dirname_1.endswith("output")

        # new call with same arguments
        filename_2 = write_output_to_file(args["output"], args["url"])
        assert filename_2

        # should use the same directory
        assert filename_1 == filename_2
        assert os.path.exists(filename_2)

    def test_makedirs_exceptions(self, caplog, args):
        with (
            patch("os.path.exists", return_value=False),
            patch("os.makedirs") as mock_makedirs,
        ):
            # mock OSError
            mock_makedirs.side_effect = OSError(errno.EACCES, "Permission denied")

            # should not create an output file
            filename = write_output_to_file(args["output"], args["url"])
            assert filename is None

        # should log the expected error
        assert "Permission denied" in caplog.text

    @pytest.mark.parametrize(
        ("error", "expected_error"),
        [
            (OSError(errno.EACCES, "Permission denied"), "Permission denied"),
            (
                OSError(errno.ENOENT, "No such file or directory"),
                "No such file or directory",
            ),
            (
                OSError(errno.ENOSPC, "No space left on device"),
                "No space left on device",
            ),
            (ValueError(), "Invalid characters in generated output"),
        ],
    )
    def test_open_exceptions(self, error, expected_error, caplog, args):
        with (
            patch("os.path.exists", return_value=True),
            patch("builtins.open", mock_open()) as mock_file_open,
        ):
            mock_file_open.side_effect = error

            # should not create an output file
            filename = write_output_to_file(args["output"], args["url"])
            assert filename is None

        # should log the expected error
        assert expected_error in caplog.text
