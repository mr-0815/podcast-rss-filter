import logging

import podfilter

logger = logging.getLogger(__name__)


class TestInit:
    def test_init_imports(self):
        assert podfilter.__all__ == ["process_feeds_from_file"]

    def test_init_documentation(self):
        assert podfilter.__name__ == "podfilter"
        assert (
            "Filter and process RSS feeds of podcasts based on specific criteria."
            in podfilter.__doc__
        )
