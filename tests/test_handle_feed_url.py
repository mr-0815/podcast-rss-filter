import logging
from unittest.mock import patch

import pytest
from feedendum import Feed
from feedendum import exceptions as feedendum_exceptions

from podfilter import app
from podfilter.validation.models import FilterModel


class TestHandleFeedUrl:
    @pytest.fixture
    def args(self, tmp_path):
        filter_args = {"title": "", "weekday": None}
        tmp_outdir = tmp_path / "output"
        return (
            {
                "url": "https://example.com/podcast.xml",
                "filters": [FilterModel(**filter_args)],
                "max_age_hours": 0,
            },
            tmp_outdir,
        )

    @pytest.fixture(autouse=True)
    def mock_write(self, args):
        """Don't actually write the output to disk."""
        args, outdir = args
        with patch(
            "podfilter.app.write_output_to_file",
            return_value=f"{outdir}/{args["url"].rsplit('/')[-1]}",
        ) as mock_write:
            yield mock_write

    def test_success(self, args):
        args, outdir = args
        # prepare mocked feed
        with open("tests/assets/feed.xml") as fh:
            feed_xml = fh.read()

        # don't actually call requests
        with patch("requests.get") as mock_get:
            mock_get.return_value.status_code = 200
            mock_get.return_value.text = feed_xml

            output_filename = app.handle_feed_url(**args)

        # should return expected filename
        assert output_filename == f"{outdir}/podcast.xml"

    @pytest.mark.parametrize(
        ("error", "expected_error_message"),
        [
            (
                feedendum_exceptions.RemoteFeedError("RemoteFeedError"),
                "An error occurred while parsing https://example.com/podcast.xml: None",
            ),
            (
                feedendum_exceptions.FeedXMLError("FeedXMLError"),
                "Got invalid data from https://example.com/podcast.xml: FeedXMLError",
            ),
            (
                feedendum_exceptions.FeedParseError("FeedParseError"),
                "Got invalid data from https://example.com/podcast.xml: FeedParseError",
            ),
        ],
    )
    def test_parse_exceptions(self, args, error, expected_error_message, caplog):
        args, _ = args
        with patch(
            "podfilter.app.from_rss_url", side_effect=error
        ) as mock_from_rss_url:
            result = app.handle_feed_url(**args)

        mock_from_rss_url.assert_called_once_with(args["url"])

        # should return the expected fail values
        assert result is None

        # should log the expected error
        assert (
            "podfilter.app",
            logging.ERROR,
            expected_error_message,
        ) in caplog.record_tuples

    @pytest.mark.parametrize(
        "error",
        [
            feedendum_exceptions.FeedParseError("FeedParseError"),
        ],
    )
    def test_to_rss_exceptions(self, args, error, caplog):
        args, _ = args
        with (
            patch("podfilter.app.from_rss_url", result=Feed()),
            patch(
                "podfilter.app.to_rss_string", side_effect=error
            ) as mock_to_rss_string,
        ):
            result = app.handle_feed_url(**args)

        mock_to_rss_string.assert_called_once()

        # should return the expected fail values
        assert result is None

        # should log the expected error
        assert (
            "podfilter.app",
            logging.ERROR,
            f"An error occurred while creating RSS string for {args["url"]}: {error}",
        ) in caplog.record_tuples
