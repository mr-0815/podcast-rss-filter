# podfilter

This Python app is designed to filter and process RSS feeds of podcasts based on specific criteria. It allows users to specify a JSON file containing the feeds to parse, an output directory, and optional filters like the episode title or publication date.

## Features

- Supports filtering feeds by episode title
- Allows for specification of a maximum age in hours for filtered feeds
- Provides logging capabilities with adjustable log levels (DEBUG, INFO, WARNING, ERROR, CRITICAL)
- By default, writes only those files to disk that have actually been changed

## Usage

### Installation

1. Clone the repo

   ```sh
   git clone https://gitlab.com/mr-0815/podcast-rss-filter.git
   cd podcast-rss-filter
   ```

2. Install the package

   - using [pip](https://pip.pypa.io/):

     ```sh
     pip install .
     ```

   - If you are using [pipx](https://pipx.pypa.io/), you can easily provide an isolated environment for the app. Install `podfilter` using this instead:

     ```sh
     pipx install .
     ```

### Standalone Invocation

To run `podfilter`, provide the required arguments (at least the `feeds.json` file):

```sh
podfilter [-h] [--outdir OUTDIR] [--write-always] [--log-level {DEBUG,INFO,WARNING,ERROR,CRITICAL}] filename
```

```sh
podfilter feeds.json
```

### Using the Python Module

```python
import podfilter

podfilter.process_feeds_from_file(
    filename: str,
    outdir: str | None = None,
    write_always: bool | None = None,
) -> list[str]
```

## `feeds.json`

The `feeds.json` file is used to store a list of podcast feeds, along with their respective titles, URLs, and filters (if any). Here's an example of what a correctly formatted JSON object in this file might look like:

```json
[
    {
        "title": "First Feed",
        "url": "https://example.com/podcast_1.xml",
        "filter": [
            {
                "title": "Only episodes containing this string in the title will be included."
            }
        ]
    },
    {
        "title": "Second Feed",
        "url": "https://example.com/podcast_2.xml",
        "hosted_url": "https://my-own-server.com/filtered-podcast-2.xml",
        "filter": [
            {
                "title": "first",
                "weekday": 3  ## "Monday is 0 and Sunday is 6"
            },
            {
                "title": "second",
                "weekday": [ 5, 6 ]  ## "can also be a list"
            }
        ],
        "max_age_hours": 24
    }
]
```

Each JSON object represents a single podcast feed. The required properties for each feed are:

- `title`: A string representing the title of the podcast. This can also be just a part of the title.
- `url`: A string representing the URL of the podcast's RSS or Atom feed.

Optional properties include:

- `filter`: An array of objects that specify filters to apply to the podcast's entries.
  - Each filter object should have a `title` property, which matches the title of an entry in the podcast. The presence and structure of this property depend on the specific podcast and its filtering requirements.
  - The `weekday` property allows to specify the day of the week on which this title should be included in the output. It expects an integer value, **where Monday is represented as 0 and Sunday as 6**. Accepts also a list.
- `max_age_hours`: An integer (or float) that specifies the time since the episode was released. Only episodes newer than this amount of hours will be included in the output.
- `hosted_url`: A string that specifies the URL, at which you will serve the output.
  - This may overwrite the feed's original URL in the output to prevent your podcatcher from bypassing your filtered feed.
  - The last part of the `hosted_url` value will also be used as the filename for the output file.

## Output

The script will generate output files in the specified directory (defaults to `./output`) for each filtered feed. Each output file is an XML file that contains the filtered feed data ready to parse by a podcatcher app.

## Used Libraries

- The main work is done by [feedendum](https://github.com/timendum/feedendum), a python library to parse and generate RSS or Atom feeds.
- [Pydantic](https://docs.pydantic.dev) is used to validate the `feeds.json` file.

## Acknowledgments

If you have any questions, suggestions, or issues, feel free to reach out.

## License

This project is licensed under the MIT License. See the LICENSE file for more information.
