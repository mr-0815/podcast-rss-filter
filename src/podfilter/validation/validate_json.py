from __future__ import annotations

import logging

from pydantic import ValidationError

from podfilter.validation.models import FeedListModel, FeedModel

logger = logging.getLogger(__name__)


def validate_json_data(data: list[FeedModel]) -> FeedListModel | None:
    """
    Use `pydantic` to parse the input data.

    Return the validated `FeedListModel` or `None`, if there was an error.
    """
    try:
        feeds = FeedListModel(feeds=data)
    except ValidationError as error:
        # generate custom error messages
        for e in error.errors():
            msg = f"ValidationError: {e.get("msg")} at {e.get("loc")}"
            if e.get("type") != "missing":
                msg += f" - input was: {e.get("input")}"
            logger.error(msg)
        return None
    else:
        logger.debug("Input validation succeeded.")
        logger.debug(feeds)
        return feeds
