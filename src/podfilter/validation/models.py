from typing import Annotated, Any

from pydantic import BaseModel, Field, HttpUrl
from pydantic.functional_validators import BeforeValidator


def convert_to_list(v: Any) -> list[Any] | None:
    """Convert the `weekday` property's entries to a list, if it isn't already, but keep a `None` value."""
    if v is not None and not isinstance(v, list):
        return [v]
    return v


class FilterModel(BaseModel):
    """Model for `pydantic`'s validation of the `filter` object."""

    title: str  # an empty string is allowed and will match all episode titles
    weekday: Annotated[
        list[Annotated[int, Field(ge=0, le=6)]] | None, BeforeValidator(convert_to_list)
    ] = None


class FeedModel(BaseModel):
    """Model for `pydantic`'s validation of the `feed` object."""

    title: str = Field(min_length=1)
    url: HttpUrl
    hosted_url: HttpUrl | None = None
    filter: list[FilterModel] | None = None
    max_age_hours: int | float = Field(default=0, ge=0)


class FeedListModel(BaseModel):
    """Model for `pydantic`'s validation of the entire `feeds` (feed list) object."""

    feeds: list[FeedModel]
