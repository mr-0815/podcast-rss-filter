import logging
from datetime import date, datetime, timedelta

import pytz

logger = logging.getLogger(__name__)


def is_older_than_hours(dt: date, hours: float) -> bool:
    """Check if a datetime object is older than the specified number of hours."""
    try:
        hours = float(hours)
    except ValueError:
        logger.error("Invalid argument: hours=%s", hours)
        return False

    now = datetime.now(pytz.timezone("UTC"))
    dt_24h_ago = now - timedelta(hours=hours)
    return dt < dt_24h_ago


def is_output_file_changed(filename: str, content: str) -> bool:
    """Check if the content of the given file would be changed."""
    result = True
    try:
        with open(str(filename)) as fh:
            if fh.read() == content:
                logger.debug("No new data for file '%s'.", filename)
                result = False
            else:
                logger.debug("File %s gets changed.", filename)
    except FileNotFoundError:
        logger.debug("File %s will be created.", filename)
    except (OSError, ValueError):
        logger.error("Error reading previous output file '%s'.", filename)

    return result
