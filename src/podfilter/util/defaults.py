from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from podfilter.util.types import ChoiceType

"""Default values. Can be overwritten by command line arguments."""
OUTDIR: str = "output"
LOG_LEVEL: ChoiceType = "INFO"
