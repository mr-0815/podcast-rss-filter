from __future__ import annotations

from typing import Literal

LOG_LEVEL_CHOICES: list[str] = ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]

type ChoiceType = Literal["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
