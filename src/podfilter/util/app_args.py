import argparse
from dataclasses import dataclass

from podfilter.util import defaults
from podfilter.util.argparse_boolean_flag import BooleanFlag
from podfilter.util.types import LOG_LEVEL_CHOICES, ChoiceType


@dataclass
class AppArgs:
    """
    The command-line arguments for the application.

    Attributes:
        filename (str): The input file name.
        log_level ("DEBUG" | "INFO" | "WARNING" | "ERROR" | "CRITICAL"): The logging level.
        outdir (str): The output directory.
        write_always (bool): Whether to always write the output.
    """

    filename: str = ""
    log_level: ChoiceType = defaults.LOG_LEVEL
    outdir: str = defaults.OUTDIR
    write_always: bool = False

    def update(self, **kwargs):
        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)


def setup_cli_args() -> AppArgs:
    """Configure command line arguments, parse it, and return a dict of the namespace."""
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="a JSON file containing the feeds to parse")
    parser.add_argument(
        "--outdir", default=defaults.OUTDIR, help="specify output directory"
    )
    parser.add_argument(
        "--write-always",
        action=BooleanFlag,
        default=False,
        help="write output file to disk, even if the content was not changed",
    )
    parser.add_argument(
        "--log-level",
        default=defaults.LOG_LEVEL,
        choices=LOG_LEVEL_CHOICES,
    )
    namespace: argparse.Namespace = parser.parse_args()

    # convert to dict
    result: AppArgs = {
        "filename": namespace.filename,
        "log_level": namespace.log_level,
        "outdir": namespace.outdir,
        "write_always": namespace.write_always,
    }
    return result
