from __future__ import annotations

import logging
from typing import TYPE_CHECKING, Any

if TYPE_CHECKING:
    from pydantic_core import Url

logger = logging.getLogger(__name__)


def parse_feed_data(
    data: dict[Any, Any], new_feed_url: Url | None = None
) -> dict[str, Any]:
    """
    Change the feed's data properties to unlink the original feed url, if possible.

    Pass a `new_feed_url` to inject into the output.

    Return the updated feed data.
    """

    # sanitize data to keep only valid (str) keys
    result = {key: value for key, value in data.items() if isinstance(key, str)}

    # replace source url in input (xmlns:itunes)
    key = "{http://www.itunes.com/dtds/podcast-1.0.dtd}new-feed-url"
    if key in result and new_feed_url:
        result[key] = str(new_feed_url)

        logger.debug("Replacing %s: %s -> %s", key, data[key], result[key])
        return result

    # replace source url in input (xmlns:atom)
    key = "{http://www.w3.org/2005/Atom}link"
    if key in result and new_feed_url:
        new_atom_link_data: str = (
            "[{'@href': '{new_feed_url}', '@rel': 'alternate'}]".replace(
                "{new_feed_url}", str(new_feed_url)
            )
        )
        result[key] = new_atom_link_data
        logger.debug("Replacing %s: %s -> %s", key, data[key], result[key])
        return result

    if new_feed_url:
        logger.info("Could not inject %s: No target found.", str(new_feed_url))

    return result
