import logging

from podfilter.util import defaults
from podfilter.util.types import LOG_LEVEL_CHOICES, ChoiceType

formatter = logging.Formatter(
    fmt="%(asctime)s %(levelname)s: %(message)s", datefmt="%Y-%m-%d %H:%M:%S"
)


def logging_basic_config(logger: logging.Logger) -> None:
    """Set logging format for specified logger."""
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)


def set_log_level(logger: logging.Logger, log_level: ChoiceType) -> None:
    """Set log level for specified logger."""
    if log_level not in LOG_LEVEL_CHOICES:
        logger.error(
            "Invalid log level: '%s'. Using default value: '%s'",
            log_level,
            defaults.LOG_LEVEL,
        )

    # convert the literal["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"] to log level value
    log_level_value = getattr(logging, str(log_level).upper(), defaults.LOG_LEVEL)

    logger.setLevel(log_level_value)


class ApplicationError(Exception):
    """Custom exception for application errors."""


class ExceptionHandler(logging.Handler):
    """Custom handler to emit an exception on errors."""

    def emit(self, record) -> None:
        if record.levelno >= logging.ERROR:
            raise ApplicationError(self.format(record))


def raise_on_error(logger: logging.Logger) -> None:
    """Add custom handler to raise an exception on errors."""
    exception_handler = ExceptionHandler()
    exception_handler.setFormatter(formatter)
    logger.addHandler(exception_handler)
