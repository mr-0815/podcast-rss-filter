import argparse


class BooleanFlag(argparse.Action):
    """
    Action for `argparse` to describe a boolean flag.

    Custom implementation of something like `argparse.BooleanOptionalAction`
    without generating the `--no-*` argument.
    """

    def __init__(self, option_strings, dest, default=False, **kwargs) -> None:  # noqa: FBT002
        super().__init__(
            option_strings=option_strings, dest=dest, nargs=0, default=default, **kwargs
        )

    def __call__(self, parser, namespace, values, option_string=None) -> None:  # noqa: ARG002
        setattr(namespace, self.dest, not self.default)
