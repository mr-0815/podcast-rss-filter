from __future__ import annotations

import json
import logging
import os
import sys
from datetime import datetime
from typing import TYPE_CHECKING, Literal

from feedendum import (
    Feed,
    FeedItem,
    from_rss_url,
    to_rss_string,
)
from feedendum import exceptions as feedendum_exceptions

from podfilter.util.app_args import AppArgs, setup_cli_args
from podfilter.util.feed_data import parse_feed_data
from podfilter.util.helpers import is_older_than_hours, is_output_file_changed
from podfilter.util.logging_setup import (
    logging_basic_config,
    raise_on_error,
    set_log_level,
)
from podfilter.validation.validate_json import validate_json_data

if TYPE_CHECKING:
    from pydantic_core import Url

    from podfilter.validation.models import FeedListModel, FilterModel

# setup AppArgs with defaults
args = AppArgs()

# define logger
logger: logging.Logger = logging.getLogger(__name__)
logging_basic_config(logger)


def get_all_feeds(json_filename: str) -> FeedListModel | None:
    """
    Load feeds from a JSON file.

    Return a list of `Feed`s or `None`, if there was an error.
    """
    logger.info("Opening feeds file: %s", json_filename)
    try:
        with open(json_filename) as fh:
            feeds_data = json.loads(fh.read())
            return validate_json_data(feeds_data)
    except FileNotFoundError:
        logger.exception("Error: File '%s' not found.", json_filename)
        return None
    except OSError as e:
        logger.error("Error reading file '{json_filename}': %s", e)
        return None
    except json.JSONDecodeError:
        logger.error("Error: File '%s' is not a valid JSON file.", json_filename)
        return None


def apply_filters(filters: list[FilterModel], item: FeedItem) -> tuple[bool, str]:
    """
    Check the given item to match the filters.

    Return if the item should be filtered out as well as the cause for that.
    """
    filter_results: list[bool] = []
    filter_cause: tuple[Literal["title", "weekday"], str] | None = None

    for item_filter in filters:
        is_filtered_out_by_filter = False

        if item_filter.title.lower() not in str(item.title).lower():
            is_filtered_out_by_filter = True
            filter_cause = ("title", item_filter.title)
        elif item_filter.weekday is not None:
            if not item.update:
                logger.warning(
                    "Ignoring invalid update object in '%s'", str(item.title)
                )
            elif all(
                item.update.weekday() != weekday for weekday in item_filter.weekday
            ):
                is_filtered_out_by_filter = True
                filter_cause = ("weekday", str(item_filter.weekday))

        filter_results.append(is_filtered_out_by_filter)

    is_filtered_out = all(filter_results)
    return is_filtered_out, str(filter_cause)


def filter_feed(
    feed: Feed,
    filters: list[FilterModel] | None,
    max_age_hours: int | float,  # noqa: PYI041
    hosted_url: Url | None = None,
) -> Feed:
    """
    Filter a feed based on the specified filters and maximum age.

    Optional argument:
        hosted_url: Used for filename creation and customizing the output data.

    Return the filtered `Feed`.
    """
    logger.info("Processing %s", feed.title)

    # prepare output feed
    output_feed = Feed()
    output_feed.title = feed.title
    output_feed._data = parse_feed_data(  # noqa: SLF001
        data=feed._data,  #  noqa: SLF001
        new_feed_url=hosted_url,
    )

    output_feed.items = []

    for item in feed.items:
        # filter old items
        if max_age_hours != 0:
            date_format = "%Y-%m-%d %H:%M:%S%z"
            try:
                item_date_dt = datetime.strptime(str(item.update), date_format)  # noqa: DTZ007
                if is_older_than_hours(item_date_dt, max_age_hours):
                    logger.debug(
                        "Discarding %s: older than %s hours", item.title, max_age_hours
                    )
                    continue
            except ValueError:
                logger.warning(
                    "Could not parse date string %s using %s. Adding this item anyway.",
                    item.update,
                    date_format,
                )

        # apply title and weekday filters
        if filters:
            is_filtered_out, cause = apply_filters(filters, item)
            if is_filtered_out:
                logger.debug("Discarding %s: filtered out by %s", item.title, cause)
                continue

        output_feed.items.append(item)
        logger.debug("Adding %s", item.title)

    return output_feed


def write_output_to_file(
    output: str,
    url: Url,
    hosted_url: Url | None = None,
    write_always_local_override: bool | None = None,
) -> str | None:
    """
    Write the filtered feed to a file.

    Generate filename from the source's `url` or the (optional) `hosted_url`
    argument.

    By default, write file only if there were changes in the file. This can be
    changed by the command line argument `--write-always` or the (optional)
    argument `write_always_local_override` (bool) to this function.

    Return the output file name or `None` if there was an error.
    """

    # check arguments passed to this function
    missing_args = [arg for arg in ("output", "url") if not locals().get(arg)]
    if missing_args:
        logger.error("Missing argument(s): %s", ", ".join(missing_args))
        return None

    outdir: str = args.outdir

    # define filename
    url_for_filename: Url = hosted_url or url
    filename = url_for_filename.path.rsplit("/")[-1] or url_for_filename.host
    full_filename: str | None = f"{outdir}/{filename}"

    # get write_always flag (fallback/default: False)
    write_always = (
        write_always_local_override
        if write_always_local_override is not None
        else args.write_always
    )

    # only write the file, if the content has changed or `write_always` was set
    if not write_always and not is_output_file_changed(str(full_filename), output):
        logger.info("File '%s' unchanged.", full_filename)
        return full_filename

    # create folder, if necessary, and write output file
    try:
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        with open(str(full_filename), "w") as fh:
            fh.write(output)
            logger.info("Writing file '%s'", full_filename)

    except OSError as e:
        logger.error("Error writing file '{full_filename}': %s", e)
        full_filename = None
    except ValueError as e:
        logger.error("Invalid characters in generated output: %s", e)
        full_filename = None
    finally:
        return full_filename  # noqa: B012


def handle_feed_url(
    url: Url,
    filters: list[FilterModel] | None,
    max_age_hours: int | float,  # noqa: PYI041
    hosted_url: Url | None = None,
) -> str | None:
    """
    Handle a feed URL by parsing the feed, filtering it, and writing it to a file.

    Return the output file name or `None` if there was an error.
    """

    # parse
    try:
        feed = from_rss_url(url)
    except feedendum_exceptions.RemoteFeedError as e:
        logger.error("An error occurred while parsing %s: %s", url, e.__cause__)
        return None
    except (
        feedendum_exceptions.FeedXMLError,
        feedendum_exceptions.FeedParseError,
    ) as e:
        logger.error("Got invalid data from %s: %s", url, e.__cause__ or str(e))
        return None

    # filter
    output_feed = filter_feed(feed, filters, max_age_hours, hosted_url=hosted_url)

    # prepare output
    try:
        output = to_rss_string(output_feed)
    except feedendum_exceptions.FeedParseError as e:
        logger.error("An error occurred while creating RSS string for %s: %s", url, e)
        return None

    # write
    return write_output_to_file(output, url, hosted_url=hosted_url)


def process_feeds_from_file(
    filename: str,
    outdir: str | None = None,
    write_always: bool | None = None,
    _is_module: bool = True,  # noqa: FBT001, FBT002
) -> list[str]:
    """
    Load feeds from a JSON file, filter them, and write the results to files.

    Arguments:
        filename (str): The input file name.
        outdir (str | None): The output directory. (default: "output")
        write_always (bool): Whether to always write the output. (default: False)

    Return a list of the output file names.
    """

    # make use of passed arguments
    if outdir:
        args.outdir = outdir
    if write_always:
        args.write_always = write_always

    # configure logging to raise exceptions, if run as module
    if _is_module:
        raise_on_error(logger)

    # get feeds
    feeds: FeedListModel | None = get_all_feeds(filename)
    if not feeds:
        logger.error("Invalid input file: %s", filename)
        if __name__ == "__main__":
            sys.exit(1)
        return []

    generated_files: list[str] = []

    # main logic
    for feed in feeds.feeds:
        file = handle_feed_url(
            url=feed.url,
            hosted_url=feed.hosted_url,
            filters=feed.filter,
            max_age_hours=feed.max_age_hours,
        )
        if file:
            generated_files.append(file)

    return generated_files


def main() -> None:
    """Entrypoint for the `podfilter` app."""

    # use args from command line
    cli_args: AppArgs = setup_cli_args()
    args.update(**cli_args)

    # customize logging
    set_log_level(logger, args.log_level)

    process_feeds_from_file(args.filename, _is_module=False)


if __name__ == "__main__":
    main()
