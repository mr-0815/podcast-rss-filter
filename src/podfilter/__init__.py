"""
Filter and process RSS feeds of podcasts based on specific criteria.

This Python app/module is designed to filter and process RSS feeds of podcasts based on specific criteria. It allows users to specify a JSON file containing the feeds to parse.

It can be used as a standalone app:
    ```
    podfilter [-h] [--outdir OUTDIR] [--write-always] [--log-level {DEBUG,INFO,WARNING,ERROR,CRITICAL}] filename
    ```

or as Python module:
    ```
    import podfilter

    podfilter.process_feeds_from_file(
        filename: str,
        outdir: str | None = None,
        write_always: bool | None = None,
    ) -> list[str]
    ```

The required argument `filename` needs to point to a `feeds.json` file, that is used to store a list of podcast feeds, along with their respective titles, URLs, and filters (if any).

See: https://gitlab.com/mr-0815/podfilter#feedsjson
"""

from podfilter.app import process_feeds_from_file

__all__ = ["process_feeds_from_file"]
